package = "simple-plugin"

version = "0.1.1-1"

-- The version '0.1.1' is the source code version, the trailing '1' is the version of this rockspec.
-- whenever the source version changes, the rockspec should be reset to 1. The rockspec version is only
-- updated (incremented) when this file changes, but the source remains the same.

supported_platforms = {"linux", "macosx"}

source = {
   url = "https://foo.self",
   tag = "0.1.1"
}

description = {
   summary = "A Kong plugin that outputs header.",
   license = "MIT"
}

build = {
   type = "builtin",
   modules = {
      ["kong.plugins.payload.handler"] = "src/handler.lua",
      ["kong.plugins.payload.schema"] = "src/schema.lua"
   }
}
