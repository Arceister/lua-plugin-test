local typedefs = require "kong.db.schema.typedefs"

return {
  name = "header-getter",
  fields = {
    { protocols = typedefs.protocols_http },
    { config = {
      type = "record",
      fields = {
        header_name = {
          type = "string",
          required = true,
          default = { "some-header" },
        },
      },
    }, },
  },
}