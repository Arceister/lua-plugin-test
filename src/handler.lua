local BasePlugin = require "kong.plugins.base_plugin"

local HeaderGetter = BasePlugin:extend()

HeaderGetter.PRIORITY = 2

function HeaderGetter:access(conf)
  local header_name = conf.header_name
  local headers = kong.request.get_headers()

  return kong.response.exit(400, {message = "Success"})
end

return HeaderGetter